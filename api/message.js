/**
 * 站内消息相关API
 */

import config from '@/config/config'
import request,{Method} from '../utils/request'

/**
 * 获取消息列表
 * @param params
 * @returns {AxiosPromise}
 */
export function getMessages(params) {
  return request.ajax({
    url: '/buyer/members/member-nocice-logs',
    method: Method.GET,
    needToken: true ,
    loading:true,
    params
  })
}

/**
 * 标记消息为已读
 * @param ids
 */
export function messageMarkAsRead(ids) {
  return request.ajax({
    url: `/buyer/members/member-nocice-logs/${ids}/read`,
    method: Method.PUT,
    needToken: true
  })
}

/**
 * 删除消息
 * @param ids
 */
export function deleteMessage(ids) {
  return request.ajax({
    url: `/buyer/members/member-nocice-logs/${ids}`,
    method: Method.DELETE,
    needToken: true
  })
}

/**
 * 获取客服IM未读消息数量
 * @returns {*}
 */
export function getUnreadChatNum() {
  return request.ajax({
    url: `${config.api.im}/buyer/im/unread-num`,
    method: Method.POST,
    needToken: true
  })
}
